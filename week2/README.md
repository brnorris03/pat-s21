# Working with the C++ AST

To make sure you have the latest code, do `git pull --recurse-submodules` in `week2`.
If working on ix-dev or apollo, feel free to use the prebuilt Clang/LLVM. To do so, 
simply `export PATH=~norris/soft/pat/bin:$PATH`. 

To build your own LLVM+Clang, follow these steps after cloning llvm-project (`git clone https://github.com/llvm/llvm-project.git`):
```
cd llvm-project
mkdir build && cd build
cmake ../llvm -DLLVM_ENABLE_PROJECTS=clang -G "Unix Makefiles" \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DLLVM_BUILD_TESTS=ON \
	-DCMAKE_INSTALL_PREFIX=$HOME/soft/pat 
make -j8
make install
make check # optional
```

This will install Clang and LLVM in `$HOME/soft/pat`, and you can then update your path to use it
with `export PATH=$HOME/soft/pat/bin:$PATH` . Note that the full installation can take over 100GB of disk space. 
To reduce the build size, you can replace `-DCMAKE_BUILD_TYPE=RelWithDebInfo` with `-DCMAKE_BUILD_TYPE=Release` and
omit the `-DLLVM_BUILD_TESTS=ON` option. The full set of options available for cmake is described
at https://llvm.org/docs/CMake.html .

## Start here!
If you are new to compilers or writing code that relies on large, complex software packages (using CMake), 
make sure to go through the __Clang Related__ section of this set of tutorials: 
https://freecompilercamp.org/clang-llvm-landing/ .

Specifically the tutorial on [Clang -- Basics of AST manipulation](https://freecompilercamp.org/clang-AST-basics/) 
would be the most useful in terms of what we are trying to do in Assignment 2. 

The rest of this README has a few more code examples and clang project templates you can use 
as a starting point for your project.

## AST Traversals
clang-babysteps : example traversal-based action implementation based on the above-mentioned
tutorial on [Clang -- Basics of AST manipulation](https://freecompilercamp.org/clang-AST-basics/).
After ensuring that llvm-config is in your PATH, the code in `week2/clang-babysteps` can be built with
```
$ cd clang-babysteps
$ mkdir build && cd build
$ cmake ..
$ make -j
Scanning dependencies of target find-class-decls
[ 50%] Building CXX object CMakeFiles/find-class-decls.dir/FindClassDecls.cpp.o
[100%] Linking CXX executable find-class-decls
[100%] Built target find-class-decls
$ ./find-class-decls "namespace n { namespace m { class C {}; } }"
Found declaration at 1:29
```

## AST Matchers
Exercises in https://clang.llvm.org/docs/LibASTMatchersTutorial.html (no code in our pat-s21 repo yet).

## clang-tool template

The [clang-tool](https://github.com/CIS-UO/clang-tool.git) template is included as a submodule in this repo.
It's a fork of this project [template](https://github.com/firolino/clang-tool) for clang-based 
tools that can be built outside the llvm-project source tree.
You may wish to read this [nice introduction](https://xinhuang.github.io/posts/2014-10-19-clang-tutorial-finding-declarations.html) 
to writing a Clang tool.

To get the clang-tool submodule sources, run `git submodule update --init` in this (pat-s21) repo.

This should populate the week2/clang-tool subdirectory. For future updates, if you wish to refresh the 
submodules in this repo, you can use `git submodule update --recursive --remote`.

## Types and functions
One option is to use the recursive AST traversals (e.g., similar to the methods used in AST Traversals above).

Another option is to use the https://github.com/foonathan/cppast.git package to work with the C++ AST at 
the level of functions (including preprocessor directives and C++ attributes). It is based on libclang 
(one of three methods for accessing and manipulating the Clang AST). You can read the author's blog post 
about it at https://foonathan.net/2017/04/cppast/. 

For the lecture exercises, first ensure that you have built Clang/LLVM following the instructions 
at https://clang.llvm.org/get_started.html. 

Next, clone the cppast repository and build the package. On ix-dev, using Prof. Norris's 
installation of Clang/LLVM, you can acccomplish this with the following commands:
```
git clone https://github.com/foonathan/cppast.git
cd cppast
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .
make -j4
```

After a successful build, you can return to the week2 directory and try it:
```
./cppast/tool/cppast example.cpp 
```

The cppast package comes with several examples you can run, e.g.,:
```
./cppast/example/cppast_example_ast_printer .
```
This will print up to function-level AST entities in the example.cpp file. 
The source files to process must be specified in the `./compile_commands.json` file, 
you can add your own sources there. You can see more information on the json 
file [here](https://clang.llvm.org/docs/JSONCompilationDatabase.html). 


