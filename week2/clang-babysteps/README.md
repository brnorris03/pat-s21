#Clang Declaration Finder

An example traversal-based action implementation based on the 
tutorial on [Clang -- Basics of AST manipulation](https://freecompilercamp.org/clang-AST-basics/).

# Building and running

After ensuring that llvm-config is in your PATH, the code in `week2/clang-babysteps` can be built with
```
$ cd clang-babysteps
$ mkdir build && cd build
$ cmake ..
$ make -j
Scanning dependencies of target find-class-decls
[ 50%] Building CXX object CMakeFiles/find-class-decls.dir/FindClassDecls.cpp.o
[100%] Linking CXX executable find-class-decls
[100%] Built target find-class-decls
$ ./find-class-decls "namespace n { namespace m { class C {}; } }"
Found declaration at 1:29
```

