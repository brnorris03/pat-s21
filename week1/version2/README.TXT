To run the lexer on an IMP program in file filename.imp:

  ./impLexer.py filename.imp

To run the parser on an IMP program in file filename.imp:

  ./impParser.py filename.imp
