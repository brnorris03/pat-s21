# Based on examples by Stephen Ferg, http://parsingintro.sourceforge.net/

''' 
IMP Grammar:

number ::= the domain of (unbounded) integer numbers, with usual operations on them

identifier ::= standard identifiers

binaryop ::= - | * | && | <=

unaryop ::= ! 

expression ::= number
    | identifier
    | expression binaryop expression
    | unaryop expression


stmt_list ::= 
    | stmt_list stmt 
    
block ::= { stmt_list }

whilestmt ::= while (expression) block

ifstmt ::= if (expression) block else block

assignstmt ::= identifier = expression;

stmt ::= assignstmt
    | ifstmt
    | whilestmt


program ::= stmt_list EOF

'''

keywords = ["if",
            "else",
            "while"
            ]

# ----------------------------------------------------------
# a list of symbols that are one character long
# ----------------------------------------------------------
EQUALS = '='
TIMES = '*'
MINUS = '-'
PLUS = '+'
NOT = '!'
LBRACE = '{'
RBRACE = '}'
LPAREN = '('
RPAREN = ')'
SEMI = ';'

oneCharSymbols = [EQUALS, TIMES, MINUS, PLUS, NOT,
                  LBRACE, RBRACE, LPAREN, RPAREN, SEMI]

# List of symbols that are more than one character long

LEQ = '<='
twoCharSymbols = [LEQ]

tokens = keywords + oneCharSymbols + twoCharSymbols

import string

IDENTIFIER_STARTCHARS = string.ascii_letters
IDENTIFIER_CHARS = string.ascii_letters + string.digits + "_"

NUMBER_STARTCHARS = "-0123456789"
NUMBER_CHARS = "0123456789"

WHITESPACE_CHARS = " \t\n"

# -----------------------------------------------------------------------
# TokenTypes for things other than symbols and keywords
# -----------------------------------------------------------------------
IDENTIFIER = "Nonterminal"
NUMBER = "Number"
WHITESPACE = "Whitespace"
EOF = "Eof"
