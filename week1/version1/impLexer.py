# Based on examples by Stephen Ferg, http://parsingintro.sourceforge.net/

#!/usr/bin/env python
print("\n A lexer (aka: Scanner, Tokenizer, Lexical Analyzer) for IMP \n")

import genericScanner as scanner
from   genericToken      import *
from   impTokens         import *

class LexerError(Exception): 
    pass

# enclose string s in double quotes
def dq(s): return '"%s"' %s


#-------------------------------------------------------------------
#
#-------------------------------------------------------------------
def initialize(sourceText):
	"""
	"""
	global scanner

	# initialize the scanner with the sourceText
	scanner.initialize(sourceText)

	# use the scanner to read the first character from the sourceText
	getChar()

#-------------------------------------------------------------------
#
#-------------------------------------------------------------------
def get():
	"""
	Construct and return the next token in the sourceText.
	"""

	#--------------------------------------------------------------------------------
	# read past and ignore any whitespace characters 
	#--------------------------------------------------------------------------------
	while c1 in WHITESPACE_CHARS:

		# process whitespace
		while c1 in WHITESPACE_CHARS:
			token = Token(character)
			token.type = WHITESPACE
			getChar() 

			while c1 in WHITESPACE_CHARS:
				token.cargo += c1
				getChar() 
						
			# return token  # only if we want the lexer to return whitespace

	# Create a new token.  The token will pick up
	# its line and column information from the character.
	# We look two characters ahead 
	token = Token(character)

	if c1 == ENDMARK:
		token.type = EOF
		return token

	if c1 in IDENTIFIER_STARTCHARS:
		token.type = IDENTIFIER
		getChar() 

		while c1 in IDENTIFIER_CHARS:
			token.cargo += c1
			getChar() 

		if token.cargo in keywords: token.type = token.cargo
		return token

	if c1 in NUMBER_STARTCHARS:
		token.type = NUMBER
		getChar() 
		
		while c1 in NUMBER_CHARS:
			token.cargo += c1
			getChar() 
		return token

	if c2 in twoCharSymbols:
		token.cargo = c2
		token.type  = token.cargo  # for symbols, the token type is same as the cargo
		getChar() # read past the first  character of a 2-character token
		getChar() # read past the second character of a 2-character token
		return token

	if c1 in oneCharSymbols:
		token.type  = token.cargo  # for symbols, the token type is same as the cargo
		getChar() # read past the symbol
		return token

	# else.... We have encountered something that we don't recognize.
	token.abort("I found a character or symbol that I do not recognize: " + dq(character.cargo))


#-------------------------------------------------------------------
#
#-------------------------------------------------------------------
def getChar():
	"""
	get the next character
	"""
	global c1, c2, character
	character     = scanner.get()
	c1 = character.cargo
	#---------------------------------------------------------------
	# Every time we get a character from the scanner, we also  
	# lookahead to the next character and save the results in c2.
	# This makes it easy to lookahead 2 characters.
	#---------------------------------------------------------------
	c2    = c1 + scanner.lookahead(1)

def writeln(*args):
	for arg in args:
		f.write(str(arg))
	f.write("\n")

#-----------------------------------------------------------------------
#
#                    main
#
#-----------------------------------------------------------------------
def main(sourceText):
	global f
	f = open(outputFilename, "w")
	writeln("Here are the tokens returned by the lexer:")

	# Initialize the lexer with the input source code
	initialize(sourceText)

	#------------------------------------------------------------------
	# use the lexer.getlist() method repeatedly to get the tokens in
	# the sourceText. Then print the tokens.
	#------------------------------------------------------------------
	while True:
		token = get()
		writeln(token.show(True))
		if token.type == EOF: break
	f.close()


if __name__ == "__main__":
	import sys, os
	outputFilename = "impLexer_output.txt"
	sourceFilename = None
	if len(sys.argv) > 1: sourceFilename = sys.argv[1]
	if not sourceFilename or not os.path.exists(sourceFilename):
		print("Usage: impLexer filename.imp")
		exit(1)
	sourceText = open(sourceFilename).read()
	main(sourceText)
	print((open(outputFilename).read()))
