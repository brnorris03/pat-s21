# CIS 410/510 Program Analysis and Transformation Term Project

The term project will involve substantial planning and implementation effort. 
The goal is to spend at least three times the amount of time and effort on your 
project than you would on a typical homework assignment in the class.

## Graduate students only

Each graduate student must find and review one or two papers related to their project. 
Asking the instructor for paper suggestions is also acceptable, once the project topic is 
selected. Graduate students must also write a short report on the paper(s) related to 
their project topic in the style of the Communications of the ACM 
"Technical Perspective" short articles.

## Timeline

* Week 6 (Sunday): 
    - Submit your proposal on Canvas by the end of week 6. A paragraph or two is sufficient,
    but provide enough detail to enable the instructor to estimate the scope and effort required.
    - Create a `project` subdirectory in your Bitbucket repo, or a new Github or Gitlab repository 
        for your projects and share the URL with the instructor
* Week 7: 
    - Finalize your project plans (consider discussing during one of the office hours) 
    and begin implementation in your project repo space. Make sure to commit and push regularly.
    15% of your project grade is based on evidence of weekly progress! 
    - Grad students: share paper info with instructor.
* Weeks 8, 9, 10: 
    - Work on implementation, commit and push regularly.
* Week 10 (Sunday): 
    - Complete work on your project; optionally give a short presentation/demo 
during our last lecture (you can continue working on the project after that).
    - Grad students only: finish report and submit through canvas.

## Topics

The following are sample project ideas; feel free to propose different ones if you have
something else you'd like to pursue. I have organized them into several broad categories.
You can choose any LLVM language/front end, or you can base your project on MLIR or the LLVM 
intermediate representation. If you are confused about the different IRs or need more 
information before choosing, take a look at this short [presentation](https://llvm.org/devmtg/2017-06/1-Davis-Chisnall-LLVM-2017.pdf).

### Bug finding, security

* Create analyses to discover common bugs in source code, e.g., similar to the ones
implemented in the [Clang Static Analyzer](https://clang-analyzer.llvm.org/). If you 
choose to reproduce some of these analysis, do NOT look at the existing implementations.
Alternatively, you may wish to extend it in some way, e.g., target another front end.

* Identify a set of static analyses that may impact security-related codes. You can use 
[The Most Dangerous Code in the World](https://crypto.stanford.edu/~dabo/pubs/abstracts/ssl-client-bugs.html)
paper for inspiration.

### Performance analysis and optimization

* Extend your work from Assignment 3 to produce more accurate performance models through 
analysis (e.g., include alias analysis, memory simulation or reuse distance model).

* Select a particular language construct, and design a new type of performance-related 
analysis and/or transformation pass. You can also focus on a particular type of computation,
e.g., tensor contractions.

### Code quality (software engineering)

* Design and implement analyses that compute several code quality metrics. You can use
this [article](https://se-education.org/learningresources/contents/codeQuality/CodeQualityMetrics.html)
as a starting point. For cohesion and coupling, [this](https://www.geeksforgeeks.org/software-engineering-coupling-and-cohesion/)
is a good summary. 

* Implement a tool that determines whether a C++ or C code has been refactored (and how), for example,
something in the spirit of this [paper](https://www.semanticscholar.org/paper/RefDiff%3A-Detecting-Refactorings-in-Version-Silva-Valente/eff97eb95de4da6cbef9e0c7a339925979afec21).

### A new analysis or transformation method

You can build on existing analyses we cover in class, or combine them in novel ways. For example,
extending existing function inlining techniques with new heuristics for deciding what and when 
to inline by using results of existing or new program analyses. Another example is focusing on a 
particular transformation, e.g., generating efficient GPU code from C or C++ (not CUDA) loops.

## Grading rubric

Grading will take into account your proposal (10%), weekly progress during weeks 8, 9, 10 (5% each), 
and the final implementation, including functionality (50% graduates, 65% undergraduates),
and documentation, including build instructions and examples (10%).

Graduate students must also write a short report on the paper(s) related to their project topic
in the style of the Communications of the ACM "Technical Perspective" short articles (15%).
