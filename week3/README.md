# Control-flow graphs.

To generate the CFG with Clang, for a source file `hello.c` and clang executables in your `PATH`.

```
clang -emit-llvm hello.c -c -o hello.bc
opt -dot-cfg-only hello.bc
```
You can view the resulting `hello.dot` file or visualizer it with graphviz, available on unix and MacOS (with Mac Ports).

You can simply look at the dot file, or visualize it with dot (part of the Graphviz package), e.g., 
```
dot -Tpng .func.dot -o func.png
```
Or you can paste the contents of `.func.dot` into an online dot file visualizer, such as https://dreampuf.github.io/GraphvizOnline/
(without having to install Graphviz locally).

For example, if `hello.c` contains the following code:
```
void func (int alpha, int n, double *x, double *y) {
   int i;
   for (int i = 0; i < n; i++) {
      y[i] = y[i] + alpha * x[i];
   }
}
```
The resulting CFG will look like this:

![CFG for function func](func.png "CFG for function func")
