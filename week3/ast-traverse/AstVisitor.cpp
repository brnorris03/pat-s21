/* A recursive visitor that visits all nodes.
 * Based on https://clang.llvm.org/docs/RAVFrontendAction.html
 */
#include <iostream>
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendAction.h>
#include <clang/Tooling/Tooling.h>
#include <llvm/Support/CommandLine.h>
#include <clang/Tooling/CommonOptionsParser.h>


#include "utils.hpp"

using namespace std;
using namespace llvm;
using namespace clang;
using namespace clang::tooling;

class AllNodesVisitor : public RecursiveASTVisitor<AllNodesVisitor> {
// This visitor visits all nodes. 
// Please refer to https://clang.llvm.org/doxygen/classclang_1_1RecursiveASTVisitor.html

public:
    explicit AllNodesVisitor(ASTContext *p_context)
      : context(p_context) {}

    bool TraverseDecl(Decl *D) {
	      cout << "Found decl: "; D->dump(); cout << endl; // D->getQualifiedNameAsString() << endl;
        // your logic here
        RecursiveASTVisitor<AllNodesVisitor>::TraverseDecl(D); // apply the base class method
        return true; // Return false to stop the AST traversal
    }
    bool TraverseStmt(Stmt *x) {
        cout << "Found stmt: "; x->dump(); cout << endl;
        // your logic here
        RecursiveASTVisitor<AllNodesVisitor>::TraverseStmt(x);
        return true;
    }
    bool TraverseType(QualType x) {
        cout << "Found type: "; x.dump(); cout << endl;
        // your logic here
        RecursiveASTVisitor<AllNodesVisitor>::TraverseType(x);
        return true;
    }
private:
    ASTContext *context;
};


class AllNodesConsumer : public clang::ASTConsumer {
public:
  explicit AllNodesConsumer(ASTContext *context)
    : visitor(context) {}

  virtual void HandleTranslationUnit(clang::ASTContext &context) {
    visitor.TraverseDecl(context.getTranslationUnitDecl());
  }
private:
  AllNodesVisitor visitor;
};

class AllNodesAction : public clang::ASTFrontendAction {
public:
  virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
    clang::CompilerInstance &compiler, llvm::StringRef inFile) {
    return std::unique_ptr<clang::ASTConsumer>(
        new AllNodesConsumer(&compiler.getASTContext()));
  }
};


static llvm::cl::OptionCategory ctCategory("ast-traverse options");

int main(int argc, const char **argv)
{

    /* From week2/clang-babysteps, this processes the source code as a string argument (not file)
    if (argc > 1) {
        clang::tooling::runToolOnCode(std::make_unique<AllNodesAction>(), argv[1]);
    }
    */

    auto expectedParser = CommonOptionsParser::create(argc, argv, ctCategory);
    if (!expectedParser) {
       // Fail gracefully for unsupported options.
       llvm::errs() << expectedParser.takeError();
       return 1;
    }
    CommonOptionsParser& optionsParser = expectedParser.get();

    for(auto &sourceFile : optionsParser.getSourcePathList())
    {
       if(utils::fileExists(sourceFile) == false)
       {
          llvm::errs() << "File: " << sourceFile << " does not exist!\n";
          return -1;
       }

       auto sourcetxt = utils::getSourceCode(sourceFile);
       auto compileCommands = optionsParser.getCompilations().getCompileCommands(getAbsolutePath(sourceFile));

       
       std::vector<std::string> compileArgs = utils::getCompileArgs(compileCommands);
       //compileArgs.push_back("-I" + utils::getClangBuiltInIncludePath(argv[0]));
       for(auto &s : compileArgs)
          llvm::outs() << s << "\n";

       auto xfrontendAction = std::make_unique<AllNodesAction>();
       utils::customRunToolOnCodeWithArgs(move(xfrontendAction), compileArgs, sourceFile);
   }
   return 0;

}

