# ASTRecursiveVisitor example

A simple AST traversal that goes through __every__ node with only three visitor methods.

## To build

Make sure llvm-config is in your path (`which llvm-config`). 

```
mkdir build && cd build
cmake ..
make
```

## To run

./ast-traverse sourcefile.cpp

