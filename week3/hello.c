void func (int alpha, int n, double *x, double *y) {
   int i;
   for (int i = 0; i < n; i++) {
      y[i] = y[i] + alpha * x[i];
   }
}
