# Schedule with reading materials

## Week 1 

* [Lecture: Intro](./week1-intro.pdf) 
    - [Mon recording](https://uoregon.zoom.us/rec/share/0bvlOTO-3r9bwJ0og9Oy8X9dFaxoqLMHuzEVuwoVgIfI04er3mIQRAsFPewWwLnG.uDLuO4eilJz3gO9Q) (password shared in Discord, same for all recordings)
    - [Wed recording](https://uoregon.zoom.us/rec/share/luo_uyevVp1ajtMFcrS-s_d9HibW8OCf6hnTR-968RVQu0ynXzHEonqIP1EYC0Pw.GEA0NkRe5wq1sV-l)

* [A simple interpreter](./01_ A Simple Interpreter.pdf)

Reading:

* PPA Chapter 1
* [Introduction to parsing with Ply](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.142.5299&rep=rep1&type=pdf)

Optional:

* [Git book](http://git-scm.com/book/en/v2)
* [A Compiler from Scratch (a Destroy All Software screencast (38 minutes)](https://www.destroyallsoftware.com/screencasts/catalog/a-compiler-from-scratch)
* [Notes on how parsers and compilers work](http://parsingintro.sourceforge.net/)

## Week 2

* [Lecture: Abstract syntax trees, Clang AST](./week2-ast-llvm.pdf)
    - [Mon recording](https://uoregon.zoom.us/rec/share/mfu_XYF5ss0wGeD2OVBpJAFnSapFzPJJsf_tTVpB_QG_j35ZXJQMVQSbzQOY1myH.04-5ceU-REx1mi68)
    - [Wed recording](https://uoregon.zoom.us/rec/share/sA8nLsvUDlUBz7Pp6zeqCwjveKmu6-1PFPO24aPiNLx3kUMo1E61lxFe8XqLwNpw.45p6rSNssMAwO_mY)
   
Reading:

* [Anatomy of a compiler](./Anatomy_of_a_compiler.pdf)
* [Introduction to the Clang AST](https://clang.llvm.org/docs/IntroductionToTheClangAST.html) 
* [Clang tutorial](https://xinhuang.github.io/posts/2014-10-19-clang-tutorial-finding-declarations.html)
* [Code transformation and analysis using Clang and LLVM (Hal Finkel and Gabor Horvath. Computer Science Summer School 2017)](ihttps://llvm.org/devmtg/2017-06/2-Hal-Finkel-LLVM-2017.pdf)

Hands-on:

* [Installing Clang/LLVM notebook](https://colab.research.google.com/drive/1_97MLACxYSlsYaO685B79Y7DeaZw875_?usp=sharing)

Assignments:

* [Assignment 2](../week2/assignment2/Assignment2.html), due Wednesday of week 4

## Week 3

* Lecture: [Control-flow analysis](./week3-cfa.pdf)
    - [Mon recording](https://uoregon.zoom.us/rec/share/Q_0c24c2cipXD44Ch-hUXaKr7koFAHu45y7OqDBaaaamX3rIv_uPHD-ATPYc7JQR.FGsJxWHnlWVjRw1Y)
    - [Wed recording](https://uoregon.zoom.us/rec/share/gPd39gzSql0KKq-xRdpk7xEssh4aipEAXL21IY94azLLjny41QnzRjWSwasL96fX.mwdjzcUfx6UlP8VH)
    
Readings:

* PPA Section 5.1

Assignments:

* Continue working on [Assignment 2](../week2/assignment2/Assignment2.html), due Wednesday of week 4

## Week 4

* Lecture: [Introduction to Dataflow Analysis](./week4-dfa.pdf)
    - [Mon recording](https://uoregon.zoom.us/rec/share/REInpa5NMuAf50nJViVLk5iXW67OuuJpgCNWydYIuMxQn_WqxcvdLsfDIMGpq9Q9.Jfb5AXELVUzkBvFM)
    - [Wed recording](https://uoregon.zoom.us/rec/share/AU4cOInWIIMPrNY610ygXLijacpGvoOXLuusWkuaYLJCA14DrPA31qIErtbhRAB8.nzRJojNLCe8M-2iw)

Reading:

* PPA Sections 2.1 -- 2.4, Appendix A (partially ordered sets)
* Chapter 6 in [Lecture Notes on Static Analysis](http://www.itu.dk/people/brabrand/static.pdf)
* [Dataflow analysis notes](http://pages.cs.wisc.edu/~horwitz/CS704-NOTES/2.DATAFLOW.html)
* [Dataflow analysis examples](https://www.cs.cmu.edu/~aldrich/courses/15-819O-13sp/resources/dataflow-examples.pdf)

Assignments:

* [Assignment 2](../week2/assignment2/Assignment2.html), due Wednesday
* [Assignment 3](../week5/assignment3/Assignment3.html), due Wednesday of week 6

## Week 5
* Lecture: [Dataflow analysis -- continuing](./week5-dfa.pdf)
    - Mon recording -- none; Zoom technical problem 
    - [Wed recording](https://uoregon.zoom.us/rec/share/Gb479NnvCFyB9zApRRDajsnptaitN884dAOxhno0OePRh1NR_VcS0jp_5mXlxdQ.805A7vhTqQNr4HoR)

Reading:

* Finish anything left from last week

Assignments:

* Continue working on [Assignment 3](../week5/assignment3/Assignment3.html), due Wednesday of week 6

## Week 6
* Lecture: [Alias analysis](./week6-alias.pdf)
    - [Mon recording](https://uoregon.zoom.us/rec/share/-FtRZpDKfUNRBI6JMQ9BdqtGEX6V3E_lqMzPkPgRmK2N4zHCjIlnYiLfE5q8mVFm.RXMqU_7OL4K8ozqe)
    - [Wed recording](https://uoregon.zoom.us/rec/share/to_7__EYF3TBn31DZq5FGK2GJbhntRJWuz7Ty5XYopoTZCFI3RHbe4L2tiIe2Jm7.hpoKONBe_4vJID6_)

Reading:

* PPA 2.4
* Optional: [An empirical study of alias analysis tecniques](https://digitalcommons.calpoly.edu/cgi/viewcontent.cgi?article=3206&context=theses)
* LLVM: 
    - [Writing an LLVM Pass](https://llvm.org/docs/WritingAnLLVMPass.html)
    - [Alias analysis infrastructure](https://llvm.org/docs/AliasAnalysis.html)

Assignments:

* [Assignment 3](../week5/assignment3/Assignment3.html) due Wednesday
* [Project proposal](../project/README.md) due Sunday (Canvas)

## Week 7
* Lectures: 
    - Monday: [Client-Driven Pointer Analysis](./week7-GuyerLin.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/rF9E-tS_qaOF2atUIXikx5SbHZ2HhoiCIB2AFWMfObcfLXQHvbuo5zQIERp9TQX-.nR_Xu_CyO-YMk9bw)
    - Wednesday: [Taint Analysis]() | [Zoom recording](https://uoregon.zoom.us/rec/share/U_OZG5jE1SsLFL2yZEJlIOMamVOR0qqEC4SWSTAEI9_XHYVxStO2m-9pkNbKSxUJ.MSUB0yPth5C2RNnj)

Reading:

* Guyer, Samuel Z. and C. Lin. [“Error checking with client-driven pointer analysis.”](https://api.semanticscholar.org/CorpusID:11669518)
Sci. Comput. Program. 58 (2005): 83-114. (1-2-hr read)
* [Taint checking](https://en.wikipedia.org/wiki/Taint_checking) (5-min read)
* LLVM:
    - HelloWorld pass example with an out-of-source build in [week7 code](../week7) .
    - [Static taint checking](https://clang.llvm.org/doxygen/GenericTaintChecker_8cpp_source.html)
    - [What is a pass manager?](https://blog.llvm.org/posts/2021-03-26-the-new-pass-manager/)
    - Optional (talk): 2019 LLVM Developers' Meeting: A. Warzynski ("Writing an LLVM Pass: 101")[https://www.youtube.com/watch?v=ar7cJl2aBuU&ab_channel=LLVM]
         - Code: https://github.com/banach-space/llvm-tutor
    
Optional:

* [Milanova, A., A. Rountev and B. Ryder. “Precise Call Graphs for C Programs with Function Pointers.” Automated Software Engineering 11 (2004): 7-26.](https://www.cs.rpi.edu/~milanova/docs/paper_kluw.pdf)


## Week 8
* Lectures: Transformations 
    - Monday: [Memory review](./week8-memory.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/XdIiT1OZ34nPxqDjE2MO_aKvDYa9UF81VJqChVEWUHjKc6e_cuOLlbkT2eKu_97v.cobrDa0PZglo8YRv)
    - Wednesday: [Loop transformations](./week8-loops.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/Rdk7Oe3Z20dTEHosfl_wohV0FxUt3KVZ60twedTxg6gxcctW9TagvlBbHtue2Pr6.UbgUl2X5Bt4cV_A5)

Reading:

* [User-directed Loop Transformations in Clang](https://sc18.supercomputing.org/proceedings/workshops/workshop_files/ws_llvmf108s2-file1.pdf)

* LLVM:
    - [Code transformation metadata](https://llvm.org/docs/TransformMetadata.html)
    - [Transform passes](https://llvm.org/docs/Passes.html#transform-passes)

## Week 9
* Lectures: Applications of program analysis and transformation
    - Monday: [Exploiting prallelism](./week9-parallel.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/2wF9yCjM8fJluDZQpAx8MBUHQr2g1GTRseisQGPeHASoc6hTIJDDlIXzxukpmorh.tc5ETbEbJH6jERUr)
    - Wednesday: [Finding bugs](./week9-bugs.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/vRIOavi5Pu-VqunkgDB7lmwR5UlZjsFK9jZRc2d5yc1qrFPBKS2IxvoVSd9sydGQ.JPnNNSN7hoJmk9qv)

Reading:

* [Introduction to Static Analysis for Assurance](./week9-rushby-sa.pdf)

## Week 10
* Lecture: [Program Analysis for Security](./week10-last.pdf) | [Zoom recording](https://uoregon.zoom.us/rec/share/mcr0DzTNXtTmB4pT3q8vrLIHAxHY-UgUY7qjXl1jdP360DBWRHJ76-Ng-Uf-RW4x.RihpRI0768N1T9PF)
