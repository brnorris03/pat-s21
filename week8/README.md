# Week 8 

Exploring transformations with Clang. This week's directory is meant to help you with Assignment 4 (on Canvas). 

I include a small example, `hello.c` and the steps required to study one transformation -- function inlining using the 	`-inline` option for `opt`.

```
// hello.c
int bar (int x) {
    return x*x;
}
int foo(int x) {
    return bar(x);
}

int main() {
    foo(2);
}

```
Then prevent optimizations in the bitcode with:
```
clang -O0 -S -emit-llvm  hello.c -o hello.ll

```
Then edit `hello.ll` and look for a line that starts with something like `attributes #0`, and remove the `noinline` and `optnone` from the entries, e.g., here is that line before:
```
attributes #0 = { noinline nounwind optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
```
and after:
```
attributes #0 = { nounwind uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
```
Then do the transformation, in this case, with inline:
```
opt -inline -inline-threshold=10000 -S hello.ll -o hello_inlined.ll 
```
To verify that `bar` was definitely inlined, compare the `hello.ll` and `hello_inlined.ll` files, e.g., originally `foo` was:
```
; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @foo(i32 %0) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = call i32 @bar(i32 %3)
  ret i32 %4
}
``` 

and in `hello_inlined.ll`, `foo` is:
```
; Function Attrs: nounwind uwtable
define dso_local i32 @foo(i32 %0) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  %4 = load i32, i32* %3, align 4
  %5 = bitcast i32* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5)
  store i32 %4, i32* %2, align 4
  %6 = load i32, i32* %2, align 4
  %7 = load i32, i32* %2, align 4
  %8 = mul nsw i32 %6, %7
  %9 = bitcast i32* %2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9)
  ret i32 %8
}
```

So we can conclude that inlining was succesful. You would also notice that `bar` was inlined in `main`.
