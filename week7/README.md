# Writing an LLVM Pass

This is a short guide for writing an LLVM pass that can be built outside the LLVM source
tree. The code is based on these [instructions](https://medium.com/@mshockwave/writing-llvm-pass-in-2018-part-i-531c700e85eb).
The differences betweeen the old and new pass managers are discussed in this
[blog](https://blog.llvm.org/posts/2021-03-26-the-new-pass-manager/) .

## Where does it go?

The easiest (and safest) location for a new LLVM pass is the LLVM source tree.
The LLVM documentation gives clear [instructions](https://llvm.org/docs/WritingAnLLVMNewPMPass.html) on how to do that, so I will not repeat 
this here. Instead, I am providing this as an example of the same kind of pass, 
but with an out-of-source build configuration.

## Building 

As usual, make sure `llvm-config` is in your path, then build with:
```
cd llvm-pass-project
mkdir build
cd build
cmake ..
make
```

## Testing

To run your pass in the llvm-pass-project directory:
```
week7/llvm-pass-project/build$ opt -disable-output \
	-load-pass-plugin=pass-example/libHelloNewPMPass.so \
	-passes="hello-new-pm-pass" ../test.ll
Hello foo
Hello bar
```

Reminder on how to produce human-readable LLVM files from C or C++ with clang:
```
clang -emit-llvm -S -c example.c -o example.ll
```

