# CFG printing  example

A simple CFG printer for Clang::CFG, in this case, it prints the CFG of the main() program only.

## To build

Make sure llvm-config is in your path (`which llvm-config`). 

```
mkdir build && cd build
cmake ..
make
```

## To run

./cfg-print example.c

