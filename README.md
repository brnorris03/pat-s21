# README #

This repository contains code examples and initial assignment code for the University of Oregon Spring 2021 course [CIS410/510 Program Analysis and Transformation](https://classes.cs.uoregon.edu/21S/cis410pat/). Most course materials are available on the official Canvas course page.

Reading materials and zoom recordings are available in the [schedule/README.md](./schedule/README.md).

Guidance on configuring and building Clang/LLVM is available in the [week2/README.md](./week2/README.md).

An informal [survey of dataflow analyses in Clang](http://clang-developers.42468.n3.nabble.com/A-survey-of-dataflow-analyses-in-Clang-td4069644.html)

## Author
Boyana Norris, norris@cs.uoregon.edu
